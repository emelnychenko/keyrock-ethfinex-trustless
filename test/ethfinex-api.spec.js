import assert from "assert"
import sinon from "sinon"
import mockrequire from "mock-require"

describe("Testing ethfinex API", () => {
  let ethfinexApi
  let ethfinexInstance

  beforeEach(() => {
    // mock object
    ethfinexInstance = {
      getOrders: sinon.fake.returns(["EthfinexAPI.getOrders"]),
      getOrdersHist: sinon.fake.returns("EthfinexAPI.getOrdersHist"),
      getOrder: sinon.fake.returns("EthfinexAPI.getOrder"),
      cancelOrder: sinon.fake.returns("EthfinexAPI.cancelOrder"),
      createOrder: sinon.fake.returns("EthfinexAPI.createOrder"),
      contract: {
        isApproved: () => 0,
        approve: sinon.fake.returns("EthfinexAPI.contract.approve"),
        locked: sinon.fake.returns("EthfinexAPI.contract.locked"),
        lock: sinon.fake.returns("EthfinexAPI.contract.lock")
      },
      web3: {
        utils: {
          fromWei: () => -1
        }
      },
      submitOrder: sinon.fake.returns("EthfinexAPI.submitOrder"),
      cancelOrders: sinon.fake.returns("EthfinexAPI.cancelOrders"),
      get: sinon.fake.returns("EthfinexAPI.getAddress"),
      account: {
        tokenBalance: sinon.fake.returns("EthfinexAPI.tokenBalance"),
        balance: sinon.fake.returns("EthfinexAPI.balance")
      },
      releaseTokens: () => sinon.fake.returns("EthfinexAPI.releaseToken")
    }

    mockrequire("../src/lib/ethfinex", {
      get: () => ethfinexInstance
    })
    ethfinexApi = require("../src/api/ethfinex")
  })

  afterEach(() => {
    ethfinexInstance = null
    ethfinexApi = null
  })

  it("Should call getOrders method", () => {
    const getOrdersSpy = sinon.spy(ethfinexApi, "getOrders")

    ethfinexApi.getOrders()

    sinon.assert.called(getOrdersSpy)

    getOrdersSpy.restore()
  })

  it("Should return getOrders with expected result", () => {
    const expectedResult = "EthfinexAPI.getOrders"

    const actualResult = ethfinexApi.getOrders()

    assert.equal(actualResult, expectedResult)
  })

  it("Should call getHistory method", () => {
    const getHistorySpy = sinon.spy(ethfinexApi, "getHistory")

    ethfinexApi.getHistory()

    sinon.assert.called(getHistorySpy)

    getHistorySpy.restore()
  })

  it("Should return getHistory with expected result", () => {
    const expectedResult = "EthfinexAPI.getOrdersHist"

    const actualResult = ethfinexApi.getHistory()

    assert.equal(actualResult, expectedResult)
  })

  it("Should call getOrder method", () => {
    const orderId = 1

    const getOrderSpy = sinon.spy(ethfinexApi, "getOrder")

    ethfinexApi.getOrder(orderId)

    assert(getOrderSpy.withArgs(orderId).calledOnce)

    getOrderSpy.restore()
  })

  it("Should return getOrder with expected result", () => {
    const orderId = 1

    const expectedResult = "EthfinexAPI.getOrder"

    const actualResult = ethfinexApi.getOrder(orderId)

    assert.equal(actualResult, expectedResult)
  })

  it("Should call cancelOrder method", () => {
    const cancelOrderId = 1

    const cancelOrderSpy = sinon.spy(ethfinexApi, "cancelOrder")

    ethfinexApi.cancelOrder(cancelOrderId)

    assert(cancelOrderSpy.withArgs(cancelOrderId).calledOnce)
  })

  it("Should return cancel order with expected result", () => {
    const cancelOrderId = 1

    const expectedResult = "EthfinexAPI.cancelOrder"

    const actualResult = ethfinexApi.cancelOrder(cancelOrderId)

    assert.equal(actualResult, expectedResult)
  })

  it("Should call createOrder method with ETH symbol", async () => {
    const symbol = "ETH"
    const amount = 1
    const price = 1

    const createOrderSpy = sinon.spy(ethfinexApi, "createOrder")

    await ethfinexApi.createOrder(symbol, amount, price)

    assert(createOrderSpy.withArgs(symbol, amount, price).calledOnce)

    createOrderSpy.restore()
  })

  it("Should call createOrder method with NOT ETH symbol", async () => {
    const symbol = "ERC20"
    const amount = 1
    const price = 1

    const createOrderSpy = sinon.spy(ethfinexApi, "createOrder")

    await ethfinexApi.createOrder(symbol, amount, price)

    assert(createOrderSpy.withArgs(symbol, amount, price).calledOnce)

    createOrderSpy.restore()
  })

  it("Should return createOrder with expected result", async () => {
    const symbol = "ETH"
    const amount = 1
    const price = 1

    const expectedResult = "EthfinexAPI.getOrder"

    const actualResult = await ethfinexApi.createOrder(symbol, amount, price)

    assert.equal(actualResult, expectedResult)
  })

  // This test is failed because createOrder throws exception
  it("Should call createOrder with exception", async () => {
    const symbol = "ETH"
    const amount = 1
    const price = 1

    // override submitOrder method with mock that returns error object
    ethfinexInstance.submitOrder = () => new Promise(resolve => resolve({ error: "Error" }))

    await ethfinexApi.createOrder(symbol, amount, price)
  })

  it("Should call cancelOrders", async () => {
    const cancelOrdersSpy = sinon.spy(ethfinexApi, "cancelOrders")

    await ethfinexApi.cancelOrders()

    sinon.assert.called(cancelOrdersSpy)

    cancelOrdersSpy.restore()
  })

  it("Should call getAddress method", () => {
    const getAddressSpy = sinon.spy(ethfinexApi, "getAddress")

    ethfinexApi.getAddress()

    sinon.assert.called(getAddressSpy)

    getAddressSpy.restore()
  })

  it("Should return getAddress with expected result", () => {
    const expectedResult = "EthfinexAPI.getAddress"

    const actualResult = ethfinexApi.getAddress()

    assert.equal(actualResult, expectedResult)
  })

  it("Should call getBalance function with argument", () => {
    const token = "ETH"

    const getBalanceSpy = sinon.spy(ethfinexApi, "getBalance")

    ethfinexApi.getBalance(token)

    assert(getBalanceSpy.withArgs(token).calledOnce)

    getBalanceSpy.restore()
  })

  it("Should call getBalance function without argument", () => {
    const getBalanceSpy = sinon.spy(ethfinexApi, "getBalance")

    ethfinexApi.getBalance()

    assert(getBalanceSpy.calledOnce)

    getBalanceSpy.restore()
  })

  it("Should call releaseToken method", async () => {
    const token = "ETH"

    const releaseTokenSpy = sinon.spy(ethfinexApi, "releaseToken")

    ethfinexApi.releaseToken(token)

    assert(releaseTokenSpy.withArgs(token).calledOnce)

    releaseTokenSpy.restore()
  })
})
