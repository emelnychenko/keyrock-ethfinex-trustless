import { get as instance } from "../lib/ethfinex"

/**
 * Get open orders
 * @returns {Promise<Object[]>}
 */
export function getOrders() {
  return instance().getOrders()
}

/**
 * Get historical orders
 * @returns {Promise<Object[]>}
 */
export function getHistory() {
  return instance().getOrdersHist()
}

/**
 * Get order by id
 * @param {string} orderId
 * @returns {Promise<Object>}
 */
export function getOrder(orderId) {
  return instance().getOrder(orderId)
}

/**
 * Cancel order by id
 * @param {string} orderId
 * @returns {Promise<void>}
 */
export function cancelOrder(orderId) {
  return instance().cancelOrder(orderId)
}

/**
 * Create new order
 * @param {string} symbol
 * @param {number} amount
 * @param {number} price
 * @param {number} lock
 * @returns {Promise<Object>}
 */
export async function createOrder(symbol, amount, price, lock = 48) {
  // Lock token
  const token0 = symbol.slice(0, 3)

  if (token0 !== "ETH") {
    const allowance = await instance().contract.isApproved(token0)
    if (allowance === 0 || allowance < amount) {
      await instance().contract.approve(token0)
    }
  }

  const locked = await instance().contract.locked(token0)
  const diff = instance().web3.utils.fromWei(locked) - amount

  if (diff < 0) {
    await instance().contract.lock(token0, Math.abs(diff), lock)
  }

  const response = await instance().submitOrder(symbol, amount, price)

  if (response instanceof Object && response.error) {
    throw new Error(JSON.stringify(response))
  }

  return getOrder(response)
}

/**
 * Cancel all orders
 * @returns {Promise<void[]>}
 */
export async function cancelOrders() {
  const orders = await getOrders()
  const queue = orders.map(order => cancelOrder(order.id))
  return Promise.all(queue)
}

/**
 * Get deposit address
 * @returns {string}
 */
export function getAddress() {
  return instance().get("account")
}

/**
 * Get default account balance or token balance
 * @param token
 * @returns {Promise<number>}
 */
export async function getBalance(token) {
  if (token) {
    return instance().account.tokenBalance(token)
  }

  return instance().account.balance()
}

/**
 * @param {string} token
 * @returns {Promise<void>}
 */
export async function releaseToken(token) {
  await instance().releaseTokens(token)
}
