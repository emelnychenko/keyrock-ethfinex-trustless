import { Router } from "express"
import { version } from "../../package.json"
import { endpointID } from "./metrics"
import wrap from "../../node_modules/express-async-wrap"

const ethfinex = require("./ethfinex")

const api = Router()

api.get("/", endpointID("root"), (req, response) => {
  response.json({ version })
})

api.delete(
  "/order/:id",
  endpointID("cancel-order"),
  wrap(async (req, response) => {
    await ethfinex.cancelOrder(req.params.id)

    response.json({ success: true })
  })
)

api.post(
  "/orders",
  endpointID("create-order"),
  wrap(async (req, response) => {
    const order = await ethfinex.createOrder(req.body.symbol, req.body.amount, req.body.price, req.body.lock)

    response.json(order)
  })
)

api.delete(
  "/orders",
  endpointID("cancel-orders"),
  wrap(async (req, response) => {
    await ethfinex.cancelOrders()

    response.json({ success: true })
  })
)

api.get(
  ["/balance", "/balance/:address"],
  endpointID("get-balance"),
  wrap(async (req, response) => {
    const balance = await ethfinex.getBalance(req.params.address)

    response.json(balance)
  })
)

api.get(
  "/orders",
  endpointID("get-orders"),
  wrap(async (req, response) => {
    const orders = await ethfinex.getOrders()

    response.json(orders)
  })
)

api.get(
  "/history",
  endpointID("get-history"),
  wrap(async (req, response) => {
    const orders = await ethfinex.getHistory()

    response.json(orders)
  })
)

api.get(
  "/address",
  endpointID("get-address"),
  wrap(async (req, response) => {
    const address = ethfinex.getAddress()

    response.json(address)
  })
)

api.post(
  "/withdraw",
  endpointID("withdraw"),
  wrap(async (req, response) => {
    await ethfinex.releaseToken(req.body.token || "ETH")

    response.json({ success: true })
  })
)

export default api
