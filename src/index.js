import config from "config"
import { logger } from "./lib/util"
import { configure as efx } from "./lib/ethfinex"
import { configure as bfx } from "./lib/ethfinex/ws"
import app from "./api"

/**
 * Configure and connect EFX and BFX clients
 * @returns {Promise<void>}
 */
async function configure() {
  await efx()
  await bfx()
}

configure().then(() => {
  const port = config.get("api.port")
  app.server.listen(port, () => {
    logger.info(`🚀 API server started on port ${port}`)
  })
})
