import config from "config"
import EFX from "efx-api-node"
import Web3 from "web3"

let instance

/**
 * @param {string} providerUri
 * @param {string} privateKey
 * @param useTestnet
 * @returns {Promise<value>}
 */
async function create(providerUri, privateKey, useTestnet) {
  const web3 = new Web3(providerUri)
  const accounts = await web3.eth.getAccounts()

  if (accounts.length <= 0) {
    throw new Error("Web3 provided have no accounts")
  }

  web3.eth.accounts.wallet.add(privateKey)
  const efx = await EFX(web3, { defaultProvider: providerUri })

  /**
   * Manual configuration for ROPSTEN TESTNET
   * https://ethfinex.docs.apiary.io/#introduction/smart-contracts
   */
  if (useTestnet) {
    const zeroExConfig = efx.config["0x"]
    // TODO: Figure out what's wrong with this address
    // zeroExConfig.exchangeAddress = "0x1D8643aaE25841322ecdE826862A9FA922770981"

    const zeroExTokenRegistry = zeroExConfig.tokenRegistry
    zeroExTokenRegistry.ETH.wrapperAddress = "0x06da2eb72279c1cec53c251bbff4a06fbfb93a5b"

    if (zeroExTokenRegistry.USD) {
      zeroExTokenRegistry.USD.wrapperAddress = "0x84442a4518126ed25a548fe3392f6021e3ccd5bb"
      zeroExTokenRegistry.USD.tokenAddress = "0x0736d0c130b2ead47476cc262dbed90d7c4eeabd"
    }

    if (zeroExTokenRegistry.ZRX) {
      zeroExTokenRegistry.ZRX.wrapperAddress = "0x39d738870db939dce5184557ce286589e62c983e"
      zeroExTokenRegistry.ZRX.tokenAddress = "0xA8E9Fa8f91e5Ae138C74648c9C304F1C75003A8D"
    }
  }

  return efx
}

/**
 * @param {string} providerUri
 * @param {string} privateKey
 * @param useTestnet
 * @returns {Promise<void>}
 */
async function configure(
  providerUri = config.get("ethfinex.providerUri"),
  privateKey = config.get("ethfinex.privateKey"),
  useTestnet = config.get("ethfinex.useTestnet")
) {
  instance = await create(providerUri, privateKey, useTestnet)
}

function get() {
  return instance
}

/**
 * @param _instance
 */
function set(_instance) {
  instance = _instance
}

module.exports = { create, configure, get, set }
