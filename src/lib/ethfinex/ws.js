import config from "config"
import BFX from "bitfinex-api-node"

/**
 * @type {WSv2|undefined}
 */
let instance

/**
 * @param {string} apiKey
 * @param {string} apiSecret
 * @param url
 * @returns {Promise<WSv2>}
 */
async function create(apiKey, apiSecret, url) {
  const bfx = new BFX()
  const ws = bfx.ws(2, { apiKey, apiSecret, url })
  await ws.open()
  return ws
}

/**
 * @param {string} apiKey
 * @param {string} apiSecret
 * @param url
 * @returns {Promise<void>}
 */
async function configure(
  apiKey = config.get("ethfinex.apiKey"),
  apiSecret = config.get("ethfinex.apiSecret"),
  url = "wss://api.ethfinex.com/ws/2"
) {
  instance = await create(apiKey, apiSecret, url)
}

/**
 * @returns {WSv2|undefined}
 */
function get() {
  return instance
}

/**
 * @param _instance
 */
function set(_instance) {
  instance = _instance
}

module.exports = { create, configure, get, set }
